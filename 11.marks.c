#include <stdio.h>
#include<math.h>
int main()
{
	int sub1,sub2,sub3,sub4,total;
	float aggr;
	printf("Enter the marks of 1st subject\n");
	scanf("%d",&sub1);
	printf("Enter the marks of 2nd subject\n");
	scanf("%d",&sub2);
	printf("Enter the marks of 3rd subject\n");
	scanf("%d",&sub3);
	printf("Enter the marks of 4th subject\n");
	scanf("%d",&sub4);
	total=sub1+sub2+sub3+sub4;
	aggr=(total*100)/400;
	printf("Sub 1= %d\n",sub1);
	printf("Sub 2= %d\n",sub2);
	printf("Sub 3= %d\n",sub3);
	printf("Sub 4= %d\n",sub4);
	printf("Total= %d\n",total);
	printf("Aggregate= %f\n",aggr);
	if(aggr>=85)
	printf("Grade: A");
	else if(aggr<85 && aggr>=60)
	printf("Grade: B");
	else if(aggr<60 && aggr>=35)
	printf("Grade: C");
	else
	printf("Grade: F");
	return 0;
}