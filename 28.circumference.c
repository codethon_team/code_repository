//Write a program to find the circumference of a circle using 4 functions
#include<math.h>

float getinput()
{
    float r;
    printf("Enter the radius:");
    scanf("%f",&r);
    return r;
}
float max(float r)
{
    float a;
    a=3.14*r*2;
    return a;
}
void output(float b)
{
    printf("The area of the circle:%f",b);
}
int main()
{   float a,b;
    printf("\t\t\tCIRCUMFRENCE OF CIRCLE\n");
    a=getinput();
    b=max(a);
    output(b);
    return 0;
    
}