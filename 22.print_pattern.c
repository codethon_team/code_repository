Write programs to print the following patterns using appropriate programming constructs.

1. @@@
   @@@
   @@@
  
  #include<stdio.h>
int main()
{
    int i,j;
    for(i=1;i<=3;i++)
    {
        printf("\n");
        for(j=1;j<=3;j++)
        printf("@");
}
return 0;
}

2. *
   **
   ***
  
  #include<stdio.h>
int main()
{
    int i,j;
    for(i=1;i<=3;i++)
    {
        printf("\n");
        for(j=1;j<=i;j++)
        printf("*");
}
return 0;
}

3. 1234
   1234
   1234
   1234

#include<stdio.h>
int main()
{
    int i,j;
    for(i=1;i<=4;i++)
    {
        printf("\n %d",i);
        for(j=1;j<=4;j++)
        printf("%d",j);
}
return 0;
}

4. 1
   12
   123
   1234
#include<stdio.h>
int main()
{
    int i,j;
    for(i=1;i<=4;i++)
    {
        printf("\n %d",i);
        for(j=1;j<=i;j++)
        printf("%d",j);
}
return 0;
}

5. 1
   22
   333
   4444
   55555

#include<stdio.h>
int main()
{
    int i,j;
    for(i=1;i<=5;i++)
    {
        printf("\n ");
        for(j=1;j<=i;j++)
        printf("%d",i);
}
return 0;
}

6. AAA
   BBB
   CCC
   #include<stdio.h>
int main()
{
    char i,j;
    for(i=65;i<=67;i++)
    {
        printf("\n ");
        for(j=65;j<=i;j++)
        printf("%c",j);
}
return 0;
}


7. A 
   ABC
   ABCDE
   ABCDEFG
#include<stdio.h>
int main()
{
    char i,j;
    for(i=65;i<=71;i=i+2)
    {
        printf("\n ");
        for(j=65;j<=i;j++)
        printf("%c",j);
}
return 0;
}


8. 0 
   12
   345
   6789
   #include<stdio.h>
int main()
{
    int i,j,count=0;
    for(i=1;i<=4;i++)
    {
        printf("\n ");
        for(j=1;j<=i;j++)
        printf("%d",count++);
}
return 0;
}


9. 1
   12
   123
   1234
   12345
   #include<stdio.h>
int main()
{
    int i,j;
    for(i=1;i<=5;i++)
    {
        printf("\n ");
        for(j=1;j<=i;j++)
        printf("%d",j);
}
return 0;
}
