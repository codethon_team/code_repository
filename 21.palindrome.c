Write a program to check whether the given number is palindrome or not.
#include <stdio.h>
int main()
{
	int n,n1,pal=0,rem,i=1;
	printf("Enter the number to check if it's Palindrome or not\n");
	scanf("%d",&n);
	n1=n;
	
	while(n!=0)
	{
		rem=n%(10);
		n=n-rem;
		n=n/10;
		
		pal=pal*(10)+rem;
		i++;
	}
	if(n1==pal)
	printf("The entered number \'%d\' is a Palindrome number %d",n1,pal);
	else 
	printf("The entered number \'%d\' is not Palindrome number %d",n1,pal);
	return 0;
}