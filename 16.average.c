Write a program to calculate the average of n numbers without using arrays.
#include <stdio.h>
#include<math.h>
 int main()
{
	int n,sum;
	float average=0.0;
	printf("Enter the value of n \n");
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
	sum=sum+i;
	printf("sum is %d\n",sum);
	average=(float)sum/n;
	printf("Average is %f \n",average);
	return 0;
}