Write a program to determine whether the given character is a vowel or a consonant.
#include <stdio.h>
 
int main()
{
	char ch;
	printf("Enter the character\n");
	scanf("%ch",&ch);
	if (ch == 'a' || ch == 'A' || ch == 'e'|| ch == 'E' || ch == 'i' || ch == 'I' ||ch =='o' || ch=='O' || ch == 'u' || ch =='U')
	printf("%c is a vowel.\n", ch);
else
  printf("%c isn't a vowel.\n", ch);

	return 0;
}