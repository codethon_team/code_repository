Write a program to calculate sum of squares of first n even numbers.#include <stdio.h>
#include<math.h> 
int main()
{
	int i,n;
	int term,sum=0;
	printf("Enter the number\n");
	scanf("%d",&n);
	for(i=1;i<=n;i++)
	{
		if(i%2==0)
		{
			term=pow(i,2);
			sum+=term;
		}
	}
printf("Sum = %d",sum);
	return 0;
}